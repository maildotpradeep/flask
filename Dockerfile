From ubuntu:latest

RUN apt-get update && apt -y install python3-pip

WORKDIR /app

COPY requirements.txt .

RUN pip3 install -r requirements.txt

COPY app.py .

ENTRYPOINT gunicorn -b 0.0.0.0:8000 app:app

EXPOSE 8000
